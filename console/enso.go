package main

import (
	command "git.fillip.dev/enso/composer/console/command"
)

// Main command-line / terminal entrypoint to the program.
func main() {
	executeCommand()
}

// executeCommand constructs and executes the terminal / CLI commands.
func executeCommand() {
	rootCommand := command.NewRootCommand()
	rootCommand.Execute()
}
