package command

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"git.fillip.dev/enso/composer/compute"
	"git.fillip.dev/enso/composer/dns"
	"git.fillip.dev/enso/composer/kv"
	"git.fillip.dev/enso/composer/pulumi/environment"
	"git.fillip.dev/enso/composer/storage"
	"git.fillip.dev/enso/composer/tls"
)

// newComposeCommand creates the terminal "compose" command for use with the command-line interface.
func newComposeCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:          "compose",
		Short:        "Composes all products",
		Example:      `  enso compose`,
		SilenceUsage: false,
	}

	command.Run = func(command *cobra.Command, arguments []string) {
		var ensoContext = context.Background()
		pulumiCommand, err := environment.Install(ensoContext)

		ensoContext = context.WithValue(ensoContext, "pulumi_command", pulumiCommand)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		// TODO: this should be workspace specific for each stack.
		err = storage.ApplyStack(ensoContext)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		err = kv.ApplyStack(ensoContext)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		err = compute.ApplyStack(ensoContext)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		err = tls.ApplyStack(ensoContext)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		err = dns.ApplyStack(ensoContext)

		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
	}

	return command
}
