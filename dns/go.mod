module git.fillip.dev/enso/composer/dns

go 1.22.4

require github.com/go-gandi/go-gandi v0.7.0
require git.fillip.dev/enso/composer/pulumi v0.0.0

require (
	github.com/peterhellberg/link v1.1.0 // indirect
	moul.io/http2curl v1.0.0 // indirect
)

replace git.fillip.dev/enso/composer/pulumi => ../pulumi