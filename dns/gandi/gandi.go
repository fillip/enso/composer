package gandi

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"

	"git.fillip.dev/enso/composer/tls/fastly"
	"github.com/go-gandi/go-gandi/config"
	"github.com/go-gandi/go-gandi/domain"
	"github.com/go-gandi/go-gandi/livedns"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Compose provides the touchpoint for the Gandi configuration composition.
type Composer struct {
	// Config is the Gandi configuration for the client.
	Config config.Config
}

// Update the DNS records on Gandi.
func (composer Composer) UpdateDNSRecords(context *pulumi.Context) error {
	domainClient := domain.New(composer.Config)

	_, err := domainClient.GetDomain("fillip.pro")

	if err != nil {
		return err
	}

	_, err = domainClient.GetNameServers("fillip.pro")

	if err != nil {
		return err
	}

	domainClient.EnableLiveDNS("fillip.pro")
	liveDNSClient := livedns.New(composer.Config)

	domainRecords := BaseDNSRecords
	domainRecords, err = appendTLSChallenges(context, domainRecords)

	if err != nil {
		return err
	}

	_, err = liveDNSClient.UpdateDomainRecords("fillip.pro", domainRecords)

	if err != nil {
		return err
	}

	return nil
}

// Appends the TLS challenges (e.g. ACME) to the DNS settings.
func appendTLSChallenges(context *pulumi.Context, dnsRecords []livedns.DomainRecord) ([]livedns.DomainRecord, error) {
	stackName := "organization/enso-tls/production"
	stackReference, err := pulumi.NewStackReference(context, stackName, nil)

	if err != nil {
		return dnsRecords, err
	}

	dnsRecords, err = appendDNSChallenges(dnsRecords, stackReference)

	if err != nil {
		return dnsRecords, err
	}

	dnsRecords, err = appendHTTPChallenges(dnsRecords, stackReference)

	if err != nil {
		return dnsRecords, err
	}

	return dnsRecords, err
}

// Appends the DNS challenges (e.g. ACME) for the DNS records.
func appendDNSChallenges(dnsRecords []livedns.DomainRecord, stackReference *pulumi.StackReference) ([]livedns.DomainRecord, error) {
	dnsChallenges, err := getDNSChallenges(stackReference)

	if err != nil {
		return nil, err
	}

	for _, challengeRecord := range dnsChallenges {
		var name string

		if challengeRecord.RecordName == "fillip.pro" {
			if challengeRecord.RecordType == "CNAME" {
				name = "*"
			} else {
				name = "@"
			}
		} else {
			name, _ = strings.CutSuffix(challengeRecord.RecordName, ".fillip.pro")
		}

		var newRecord = livedns.DomainRecord{
			RrsetType:   challengeRecord.RecordType,
			RrsetName:   name,
			RrsetValues: challengeRecord.RecordValues,
		}

		dnsRecords = append(dnsRecords, newRecord)
	}

	return dnsRecords, err
}

// Gets the DNS (ACME) challenges for the TLS certificates.
func getDNSChallenges(stackReference *pulumi.StackReference) ([]fastly.ACMEChallenge, error) {
	httpChallenges, err := getACMEChallenges("tlsDNSChallengeDetails", stackReference)

	return httpChallenges, err
}

// Appends the HTTP (ACME) challenges for the TLS certificates.
func appendHTTPChallenges(dnsRecords []livedns.DomainRecord, stackReference *pulumi.StackReference) ([]livedns.DomainRecord, error) {
	httpChallenges, err := getHTTPChallenges(stackReference)

	if err != nil {
		return nil, err
	}

	for _, challengeRecord := range httpChallenges {
		var name string
		if challengeRecord.RecordName == "fillip.pro" {
			if challengeRecord.RecordType == "CNAME" {
				name = "*"
			} else {
				name = "@"
			}
		} else {
			name, _ = strings.CutSuffix(challengeRecord.RecordName, ".fillip.pro")
		}

		var newRecord = livedns.DomainRecord{
			RrsetType:   challengeRecord.RecordType,
			RrsetName:   name,
			RrsetValues: challengeRecord.RecordValues,
		}

		dnsRecords = append(dnsRecords, newRecord)
	}

	return dnsRecords, err
}

// Gets the HTTP challenges (e.g. ACME) for the TLS certificates.
func getHTTPChallenges(stackReference *pulumi.StackReference) ([]fastly.ACMEChallenge, error) {
	httpChallenges, err := getACMEChallenges("tlsHTTPChallengeDetails", stackReference)

	return httpChallenges, err
}

// Gets the ACME challenges for the TLS certificates.
func getACMEChallenges(referenceName string, stackReference *pulumi.StackReference) ([]fastly.ACMEChallenge, error) {
	var acmeChallenges []fastly.ACMEChallenge

	acmeChallenge := stackReference.GetOutput(pulumi.String(referenceName))

	var waitGroup sync.WaitGroup
	waitGroup.Add(1)

	_ = acmeChallenge.ApplyT(func(v interface{}) []fastly.ACMEChallenge {
		var challenges []fastly.ACMEChallenge

		if v == nil {
			fmt.Println("ACME challenges not found")
			return challenges
		}

		rawOutput := []byte(v.(string))

		if len(rawOutput) == 0 {
			return acmeChallenges
		}

		err := json.Unmarshal(rawOutput, &acmeChallenges)

		if err != nil {
			fmt.Printf("could not unmarshall acme challenges: %v", err.Error())
		}

		challenges = acmeChallenges

		waitGroup.Done()

		return challenges
	}).(pulumi.AnyOutput)

	waitGroup.Wait()

	return acmeChallenges, nil
}
