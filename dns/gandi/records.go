package gandi

import "github.com/go-gandi/go-gandi/livedns"

// Base DNS records.
// TODO: create a separate DNS record that can be embedded, and separate across the various domains.
var BaseDNSRecords = []livedns.DomainRecord{
	{
		RrsetType:   "CNAME",
		RrsetName:   "27197545",
		RrsetValues: []string{"sendgrid.net."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "em3414",
		RrsetValues: []string{"u27197545.wl046.sendgrid.net."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "fm1._domainkey",
		RrsetValues: []string{"fm1.fillip.pro.dkim.fmhosted.com."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "fm2._domainkey",
		RrsetValues: []string{"fm2.fillip.pro.dkim.fmhosted.com."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "fm3._domainkey",
		RrsetValues: []string{"fm3.fillip.pro.dkim.fmhosted.com."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "s1._domainkey",
		RrsetValues: []string{"s1.domainkey.u27197545.wl046.sendgrid.net."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "s2._domainkey",
		RrsetValues: []string{"s2.domainkey.u27197545.wl046.sendgrid.net."},
	},
	{
		RrsetType:   "CNAME",
		RrsetName:   "url5580",
		RrsetValues: []string{"sendgrid.net."},
	},
	{
		RrsetType:   "MX",
		RrsetName:   "@",
		RrsetValues: []string{"10 in1-smtp.messagingengine.com.", "20 in2-smtp.messagingengine.com."},
	},
	{
		RrsetType:   "TXT",
		RrsetName:   "@",
		RrsetValues: []string{"\"v=spf1 include:spf.messagingengine.com ?all\""},
	},
	{
		RrsetType:   "TXT",
		RrsetName:   "krs._domainkey.mailing",
		RrsetValues: []string{"\"k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQbPZCCOZm4IJlaBE+ijD9Hd3HMpZOgAOYVQibz1Ik0MaIORTh8RQzNuwyjLWiRPSNUn5cV4bqFupDgDnTyMi0h9CbMeo6UHJ0N8P62Ff1J8uGl6kok7rqDYIA61/IRpY/fPHqukFAcqaaQ1RIbtGykPJq72I6oGIGK8y76s1OuwIDAQAB\""},
	},
	{
		RrsetType:   "TXT",
		RrsetName:   "mailing",
		RrsetValues: []string{"\"v=spf1 include:mailgun.org ~all\""},
	},
}
