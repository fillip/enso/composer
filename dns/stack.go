package dns

import (
	"context"
	"fmt"
	"os"

	"git.fillip.dev/enso/composer/dns/gandi"
	"git.fillip.dev/enso/composer/pulumi/stack"
	"git.fillip.dev/enso/composer/pulumi/state"
	"github.com/go-gandi/go-gandi/config"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Applies a Pulumi stack for a given environment.
func ApplyStack(context context.Context) error {
	stack, err := CreateStack(context)

	if err != nil {
		return err
	}

	result, err := stack.Up(context)

	if err != nil {
		return err
	}

	fmt.Printf("%+v\n", result.Summary.ResourceChanges)

	return err
}

// Createa a Pulumi stack for a given environment.
func CreateStack(context context.Context) (*auto.Stack, error) {
	// Define organization ID, project name, and stack name
	const orgId = "organization"
	const projectName = "enso-dns"
	const stackName = "production"

	// Combine organization ID with project and stack names
	fullProjectName := fmt.Sprintf("%s/%s", orgId, projectName)
	fullStackName := fmt.Sprintf("%s/%s", fullProjectName, stackName)

	project := state.CreateProject(projectName)
	pulumiProject, err := project.GetPulumiProject()

	if err != nil {
		return nil, err
	}

	// Create a new stack. You will be prompted to choose your credentials.
	autoStack, err := auto.UpsertStackInlineSource(context, fullStackName, fullProjectName, composeDNS, auto.Project(*pulumiProject))

	if err != nil {
		return nil, err
	}

	stack.SetBaseConfig(context, &autoStack)

	return &autoStack, err
}

// Composes the DNS records update.
func composeDNS(context *pulumi.Context) error {

	// TODO: check this at the root of the program.
	clientConfig := config.Config{
		PersonalAccessToken: os.Getenv("GANDI_PAT"),
		DryRun:              true,
	}

	gandiComposer := gandi.Composer{
		Config: clientConfig,
	}

	err := gandiComposer.UpdateDNSRecords(context)

	if err != nil {
		return err
	}

	return nil
}
