package tls

import (
	"context"
	"fmt"

	"git.fillip.dev/enso/composer/pulumi/stack"
	"git.fillip.dev/enso/composer/pulumi/state"
	"git.fillip.dev/enso/composer/tls/fastly"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Applies a given composed stack for an environment.
func ApplyStack(context context.Context) error {
	stack, err := CreateStack(context)

	if err != nil {
		return err
	}

	result, err := stack.Up(context)

	if err != nil {
		return err
	}

	fmt.Printf("%+v\n", result.Summary.ResourceChanges)

	return err
}

// Create a new stack for a given environment.
func CreateStack(context context.Context) (*auto.Stack, error) {
	// Define organization ID, project name, and stack name
	const orgId = "organization"
	const projectName = "enso-tls"
	const stackName = "production"

	// Combine organization ID with project and stack names
	fullProjectName := fmt.Sprintf("%s/%s", orgId, projectName)
	fullStackName := fmt.Sprintf("%s/%s", fullProjectName, stackName)

	project := state.CreateProject(projectName)
	pulumiProject, err := project.GetPulumiProject()

	if err != nil {
		return nil, err
	}

	// Create a new stack. You will be prompted to choose your credentials.
	autoStack, err := auto.UpsertStackInlineSource(context, fullStackName, fullProjectName, composeTLS, auto.Project(*pulumiProject))

	if err != nil {
		return nil, err
	}

	stack.SetBaseConfig(context, &autoStack)

	return &autoStack, err
}

// Compose the desired TLS subscription state.
func composeTLS(context *pulumi.Context) error {
	tlsSubscription, err := fastly.ComposeTLSSubscriptions(context)

	if err != nil {
		return err
	}

	context.Export("tlsSubscriptionId", tlsSubscription.ID())
	context.Export("tlsDNSChallengeDetails", pulumi.All(tlsSubscription.ManagedDnsChallenges).ApplyT(fastly.GetDNSChallengeDetails))
	context.Export("tlsHTTPChallengeDetails", pulumi.All(tlsSubscription.ManagedHttpChallenges).ApplyT(fastly.GetHTTPChallengeDetails))

	return nil
}
