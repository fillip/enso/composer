package fastly

import (
	"sync"
	"testing"

	"github.com/pulumi/pulumi/sdk/v3/go/common/resource"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/stretchr/testify/assert"
)

type mocks int

func (mocks) NewResource(args pulumi.MockResourceArgs) (string, resource.PropertyMap, error) {
	return args.Name + "_id", args.Inputs, nil
}

func (mocks) Call(args pulumi.MockCallArgs) (resource.PropertyMap, error) {
	return args.Args, nil
}

func TestComposeTLSSubscription(t *testing.T) {
	err := pulumi.RunErr(
		func(pulumiContext *pulumi.Context) error {
			bucket, err := ComposeTLSSubscriptions(pulumiContext)
			assert.NoError(t, err)

			var waitGroup sync.WaitGroup
			waitGroup.Add(1)

			pulumi.All(bucket.URN()).
				ApplyT(
					func(all []interface{}) error {
						urn := all[0].(pulumi.URN)

						assert.NotEmpty(t, urn, "URN not set")
						waitGroup.Done()

						return nil
					})

			waitGroup.Wait()
			return nil
		},
		pulumi.WithMocks("project", "stack", mocks(0)),
	)
	assert.NoError(t, err)
}
