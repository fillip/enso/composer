package fastly

import (
	"encoding/json"
	"fmt"

	"github.com/pulumi/pulumi-fastly/sdk/v8/go/fastly"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// List of domains to register for TLS certificates.
var domains = []string{
	"fillip.pro", "*.fillip.pro",
}

// ACME challenge for TLS certificate registrations.
type ACMEChallenge struct {
	// Domain name.
	Domain string `json:"domain"`
	// Record name (DNS).
	RecordName string `json:"name"`
	// Record type (DNS, e.g. TXT).
	RecordType string `json:"type"`
	// Record values (DNS: list of values).
	RecordValues []string `json:"challenges"`
}

// Compose the desired state for a TLS subscription on Fastly.
func ComposeTLSSubscriptions(context *pulumi.Context) (*fastly.TlsSubscription, error) {
	tlsSubscription, err := fastly.NewTlsSubscription(context, "fillip.pro TLS subscription", &fastly.TlsSubscriptionArgs{
		CertificateAuthority: pulumi.String("lets-encrypt"),
		CommonName:           pulumi.String("fillip.pro"),
		Domains:              pulumi.ToStringArray(domains),
	})

	if err != nil {
		return nil, err
	}

	var dnsChallenges = pulumi.
		All(tlsSubscription.ManagedDnsChallenges).
		ApplyT(GetDNSChallengeDetails)

	var httpChallenges = pulumi.
		All(tlsSubscription.ManagedHttpChallenges).
		ApplyT(GetHTTPChallengeDetails)

	context.Export("dnsChallengeDetails", pulumi.Any(dnsChallenges))
	context.Export("httpChallengeDetails", pulumi.Any(httpChallenges))

	return tlsSubscription, err
}

// Get the DNS challenge details from the subscription as a string.
func GetDNSChallengeDetails(outputs []interface{}) string {
	var challenges = make([]ACMEChallenge, len(outputs))

	for _, rawChallenge := range outputs {
		rawChallenges := rawChallenge.([]fastly.TlsSubscriptionManagedDnsChallenge)

		for index, challenge := range rawChallenges {
			fmt.Printf("%v\n", *challenge.RecordName)
			challenges[index] = ACMEChallenge{
				Domain:       "fillip.pro",
				RecordName:   *challenge.RecordName,
				RecordType:   *challenge.RecordType,
				RecordValues: []string{*challenge.RecordValue},
			}
		}
	}

	fmt.Printf("found %v challenges\n", len(challenges))

	json, _ := json.Marshal(challenges)

	return string(json)
}

// Get the HTTP challenge details (ACME) as a string.
func GetHTTPChallengeDetails(outputs []interface{}) string {
	if len(outputs) == 0 {
		return ""
	}

	var challenges []ACMEChallenge

	for _, rawChallenge := range outputs {
		rawChallenges := rawChallenge.([]fastly.TlsSubscriptionManagedHttpChallenge)

		for _, challenge := range rawChallenges {

			fmt.Printf("name %v \n", *challenge.RecordName)

			challenges = append(challenges, ACMEChallenge{
				Domain:       "fillip.pro",
				RecordName:   *challenge.RecordName,
				RecordType:   *challenge.RecordType,
				RecordValues: challenge.RecordValues,
			})
		}
	}

	fmt.Printf("found %v challenges\n", len(challenges))

	json, _ := json.Marshal(challenges)

	return string(json)
}
