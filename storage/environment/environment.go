package environment

import (
	"context"

	"github.com/pulumi/pulumi/sdk/v3/go/auto"
)

type Environment struct {
	workspace auto.Workspace
}

func NewEnvironment(workspace auto.Workspace) *Environment {
	return &Environment{
		workspace: workspace,
	}
}

func (environment Environment) InstallPlugin(context context.Context, name string, version string) error {
	err := environment.workspace.InstallPlugin(context, name, version)

	if err != nil {
		return err
	}

	return err
}
