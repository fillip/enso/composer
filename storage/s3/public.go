package s3

import (
	"github.com/pulumi/pulumi-aws/sdk/v6/go/aws/iam"
	"github.com/pulumi/pulumi-aws/sdk/v6/go/aws/s3"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Gets an existing bucket (if exists) for an update.
func GetBucketForPublicContent(context *pulumi.Context) (*s3.BucketV2, error) {
	bucketInfo, err := s3.LookupBucket(context, &s3.LookupBucketArgs{
		Bucket: "public.eu.fillip.pro",
	})

	if err != nil {
		return nil, err
	}

	bucket, err := s3.GetBucketV2(context, bucketInfo.Bucket, pulumi.ID(bucketInfo.Id), nil)

	if err != nil {
		return nil, err
	}

	return bucket, err
}

// Creates a new S3 bucket for public content.
func CreateBucketForPublicContent(context *pulumi.Context) (*s3.BucketV2, error) {
	// Create an S3 bucket resource in Wasabi.
	bucket, err := s3.NewBucketV2(context, "public.eu.fillip.pro", &s3.BucketV2Args{
		Acl:    pulumi.String("private"),
		Bucket: pulumi.String("public.eu.fillip.pro"),
		Loggings: s3.BucketV2LoggingArray{
			s3.BucketV2LoggingArgs{
				TargetBucket: pulumi.String("fillip.pro"),
				TargetPrefix: pulumi.String(".logs/"),
			},
		},
		Versionings: s3.BucketV2VersioningArray{
			s3.BucketV2VersioningArgs{
				Enabled: pulumi.Bool(true),
			},
		},
	})

	return bucket, err
}

func CreatePolicyDocumentForPublicContent(context *pulumi.Context) (iam.GetPolicyDocumentResultOutput, error) {
	policyDocument := iam.GetPolicyDocumentOutput(context, iam.GetPolicyDocumentOutputArgs{
		Statements: iam.GetPolicyDocumentStatementArray{
			iam.GetPolicyDocumentStatementArgs{
				Principals: iam.GetPolicyDocumentStatementPrincipalArray{
					iam.GetPolicyDocumentStatementPrincipalArgs{
						Type: pulumi.String("AWS"),
						Identifiers: pulumi.StringArray{
							pulumi.String("arn:aws:iam::100000214856:user/p.j@fillip.pro"),
						},
					},
				},
				Actions: pulumi.StringArray{
					pulumi.String("s3:GetObject"),
					pulumi.String("s3:ListBucket"),
					pulumi.String("s3:PutObject"),
					pulumi.String("s3:PutObjectAcl"),
				},
				Resources: pulumi.StringArray{
					pulumi.String("arn:aws:s3:::public.eu.fillip.pro/*"),
				},
			},
		},
	})

	return policyDocument, nil
}

func CreatePolicyForPublicBucket(context *pulumi.Context, policyDocument iam.GetPolicyDocumentResultOutput) (*s3.BucketPolicy, error) {
	stackName := context.Stack()
	other, err := pulumi.NewStackReference(context, stackName, nil)
	if err != nil {
		return nil, err
	}

	_ = other.GetOutput(pulumi.String("publicBucketId"))

	bucketPolicy, err := s3.NewBucketPolicy(context, "public.eu.fillip.pro_acl_owners", &s3.BucketPolicyArgs{
		Bucket: pulumi.String("public.eu.fillip.pro"),
		Policy: policyDocument.ApplyT(func(policyDocument iam.GetPolicyDocumentResult) (*string, error) {
			return &policyDocument.Json, nil
		}).(pulumi.StringPtrOutput),
	})

	return bucketPolicy, err
}
