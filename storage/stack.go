package storage

import (
	"context"
	"fmt"

	pulumiEnvironment "git.fillip.dev/enso/composer/pulumi/environment"
	"git.fillip.dev/enso/composer/pulumi/stack"
	"git.fillip.dev/enso/composer/pulumi/state"
	"git.fillip.dev/enso/composer/storage/s3"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Applies the composed stack for a given environment.
func ApplyStack(context context.Context) error {
	stack, err := CreateStack(context)

	if err != nil {
		return err
	}

	result, err := stack.Up(context)

	if err != nil {
		return err
	}

	fmt.Printf("%+v\n", result.Summary.ResourceChanges)

	return err
}

// Creates a stack for a given environment.
func CreateStack(context context.Context) (*auto.Stack, error) {
	// Define organization ID, project name, and stack name
	const orgId = "organization"
	const projectName = "enso-storage"
	const stackName = "production"

	// Combine organization ID with project and stack names
	fullProjectName := fmt.Sprintf("%s/%s", orgId, projectName)
	fullStackName := fmt.Sprintf("%s/%s", fullProjectName, stackName)

	project := state.CreateProject(projectName)
	pulumiProject, err := project.GetPulumiProject()

	if err != nil {
		return nil, err
	}

	pulumiCommand, err := pulumiEnvironment.GetCommand(context)

	if err != nil {
		return nil, err
	}

	// Create a new stack. You will be prompted to choose your credentials.
	autoStack, err := auto.UpsertStackInlineSource(context, fullStackName, fullProjectName, composeStorage, auto.Pulumi(*pulumiCommand), auto.Project(*pulumiProject))

	if err != nil {
		return nil, err
	}

	stack.SetBaseConfig(context, &autoStack)
	//autoStack.Workspace().InstallPlugin()

	return &autoStack, err
}

func composeStorage(context *pulumi.Context) error {
	bucket, err := s3.CreateBucketForPublicContent(context)
	if err != nil {
		return err
	}

	policyDocument, err := s3.CreatePolicyDocumentForPublicContent(context)
	if err != nil {
		return err
	}

	context.Export("publicBucketId", bucket.ID())

	_, err = s3.CreatePolicyForPublicBucket(context, policyDocument)
	if err != nil {
		return err
	}

	return nil
}
