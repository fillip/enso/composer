package fastly

import (
	"fmt"

	"github.com/pulumi/pulumi-fastly/sdk/v8/go/fastly"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Composes the state for the key-value store setup on Fastly.
func ComposeKVStore(context *pulumi.Context) (*fastly.Kvstore, error) {
	const kvName = "kv.eu.fillip.pro"

	var kvStore *fastly.Kvstore
	var kvId string

	kvStores, err := fastly.GetKvstores(context)

	if err != nil {
		return nil, err
	}

	// Iterate through each service, looking up the one that matches the name
	// we're going to use for this compute service.
	//
	// If we find it then we'll reuse the identity for this stack. This enables
	// the program to re-reference an existing stack if the stack state has been
	// lost, or for services created manually, prior to automation.
	for _, kvStoreItem := range kvStores.Stores {
		if kvStoreItem.Name == kvName {
			kvId = kvStoreItem.Id
			kvStore, err = fastly.GetKvstore(context, kvStoreItem.Name, pulumi.ID(kvStoreItem.Id), nil, nil)

			if err != nil {
				return nil, err
			}

			fmt.Printf("found kv store %v\n", kvId)

			break
		}
	}

	// Resource options enables us to make additional conditions for a new stack,
	// in this case we're primarily using it to potentially import an existing stack.
	var resourceOption pulumi.ResourceOption

	// If a service identity exists from a previous service then import it into
	// our resource option when creating this new stack.
	if len(kvId) != 0 {
		//resourceOption = pulumi.Import(kvStore.ID()) //pulumi.ID(kvId))
	} else {
		kvStore, err = fastly.NewKvstore(context, "fillip-eu-kv-store", &fastly.KvstoreArgs{
			Location: pulumi.String("EU"),
			Name:     pulumi.String("kv.eu.fillip.pro"),
		}, resourceOption)
	}

	if err != nil {
		return kvStore, err
	}

	return kvStore, err
}
