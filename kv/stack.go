package kv

import (
	"context"
	"fmt"

	"git.fillip.dev/enso/composer/kv/fastly"
	"git.fillip.dev/enso/composer/pulumi/stack"
	"git.fillip.dev/enso/composer/pulumi/state"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Applies a composed stack for a given environment with Pulumi.
func ApplyStack(context context.Context) error {
	stack, err := CreateStack(context)

	if err != nil {
		return err
	}

	result, err := stack.Up(context)

	if err != nil {
		return err
	}

	fmt.Printf("%+v\n", result.Summary.ResourceChanges)

	return err
}

// Creates a stack for a given environment with Pulumi.
func CreateStack(context context.Context) (*auto.Stack, error) {
	// Define organization ID, project name, and stack name
	const orgId = "organization"
	const projectName = "enso-kv"
	const stackName = "production"

	// Combine organization ID with project and stack names
	fullProjectName := fmt.Sprintf("%s/%s", orgId, projectName)
	fullStackName := fmt.Sprintf("%s/%s", fullProjectName, stackName)

	project := state.CreateProject(projectName)
	pulumiProject, err := project.GetPulumiProject()

	if err != nil {
		return nil, err
	}

	// Create a new stack. You will be prompted to choose your credentials.
	autoStack, err := auto.UpsertStackInlineSource(context, fullStackName, fullProjectName, composeKV, auto.Project(*pulumiProject))

	if err != nil {
		return nil, err
	}

	stack.SetBaseConfig(context, &autoStack)
	//autoStack.Workspace().InstallPlugin()

	return &autoStack, err
}

func composeKV(context *pulumi.Context) error {
	kvStore, err := fastly.ComposeKVStore(context)
	if err != nil {
		return err
	}

	context.Export("euKVStoreID", kvStore.ID())

	return nil
}
