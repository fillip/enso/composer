package compute

import (
	"context"
	"fmt"

	"git.fillip.dev/enso/composer/compute/fastly"
	"git.fillip.dev/enso/composer/pulumi/stack"
	"git.fillip.dev/enso/composer/pulumi/state"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Applies the compute stack to an environment.
func ApplyStack(context context.Context) error {
	stack, err := CreateStack(context)

	if err != nil {
		return err
	}

	result, err := stack.Up(context)

	fmt.Printf("%+v\n", result.Summary.ResourceChanges)

	if err != nil {
		return err
	}

	return err
}

// Creates a stack to be committed to an environment.
func CreateStack(context context.Context) (*auto.Stack, error) {
	// TODO: provide options for alternative stacks / environments.
	// Define organization ID, project name, and stack name
	const orgId = "organization"
	const projectName = "enso-edge-compute"
	const stackName = "production"

	// Combine organization ID with project and stack names
	fullProjectName := fmt.Sprintf("%s/%s", orgId, projectName)
	fullStackName := fmt.Sprintf("%s/%s", fullProjectName, stackName)

	project := state.CreateProject(projectName)
	pulumiProject, err := project.GetPulumiProject()

	if err != nil {
		return nil, err
	}

	// Create a new stack. You will be prompted to choose your credentials.
	autoStack, err := auto.UpsertStackInlineSource(context, fullStackName, fullProjectName, composeCompute, auto.Project(*pulumiProject))

	if err != nil {
		return nil, err
	}

	stack.SetBaseConfig(context, &autoStack)
	//autoStack.Workspace().InstallPlugin()

	return &autoStack, err
}

func composeCompute(context *pulumi.Context) error {
	compute, err := fastly.ComposeCompute(context)
	if err != nil {
		return err
	}

	context.Export("fastlyEdgeComputeId", compute.ID())

	return nil
}
