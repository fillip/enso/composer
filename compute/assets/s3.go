package assets

import (
	"context"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

// BucketBasics encapsulates the Amazon Simple Storage Service (Amazon S3) actions
// used in the examples.
// It contains S3Client, an Amazon S3 service client that is used to perform bucket
// and object actions.
type BucketBasics struct {
	S3Client *s3.Client
}

// DownloadFile gets an object from a bucket and stores it in a local file.
func (basics BucketBasics) DownloadFile(bucketName string, objectKey string, fileName string) (string, error) {
	result, err := basics.S3Client.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})

	if err != nil {
		log.Printf("Couldn't get object %v:%v. Here's why: %v\n", bucketName, objectKey, err)
		return "", err
	}

	defer result.Body.Close()

	file, err := os.Create(fileName)

	if err != nil {
		log.Printf("Couldn't create file %v. Here's why: %v\n", fileName, err)
		return "", err
	}

	defer file.Close()

	body, err := io.ReadAll(result.Body)

	if err != nil {
		log.Printf("Couldn't read object body from %v. Here's why: %v\n", objectKey, err)
		return "", err
	}

	_, err = file.Write(body)

	if err != nil {
		log.Printf("Couldn't write object body to %v. Here's why: %v\n", objectKey, err)
		return "", err
	}

	info, err := file.Stat()

	if err != nil {
		log.Printf("Couldn't get info for file %v. Here's why: %v\n", objectKey, err)
		return "", err
	}

	path, err := filepath.Abs(info.Name())

	if err != nil {
		log.Printf("Couldn't get full path for file %v. Here's why: %v\n", objectKey, err)
		return "", err
	}

	return path, err
}
