package fastly

import (
	"context"
	"os"

	"git.fillip.dev/enso/composer/compute/assets"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/pulumi/pulumi-fastly/sdk/v8/go/fastly"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Compose the Fastly Compute service stack for traffic to the root Fillip.pro domain.
//
// If successful it returns the new _ServiceCompute_. On failure, an error is returned.
//
// This stack relies on the key-value store being instantiated successfully first.
func ComposeCompute(context *pulumi.Context) (*fastly.ServiceCompute, error) {

	const serviceName = "Fillip Edge Compute"

	// Reference the existing KV stack to use as part of this compute.
	stackName := "organization/enso-kv/production"
	other, err := pulumi.NewStackReference(context, stackName, nil)
	if err != nil {
		return nil, err
	}

	// The KV identity should be already been exported prior to this stack.
	kvStoreId := other.GetOutput(pulumi.String("euKVStoreID"))

	// Get the WebAssembly package from storage to use in this stack.
	// The package must be in the Fastly format using `fastly compute build`,
	// which will result in a compressed archive. The .wasm package is not
	// enough.
	wasmPackageName, err := getComputePackage()

	if err != nil {
		return nil, err
	}

	// Get the hash of the WebAssembly package for Fastly to upload.
	//
	// If the hash is not used in the stack creation then the delta between
	// packages is more difficult to verify, and often the stack will not update.
	//
	// The hash enables the previous package to be compared within the state to
	// guarantee updates when only the WebAssembly module has changed.
	hash, err := fastly.GetPackageHash(context, &fastly.GetPackageHashArgs{
		Filename: &wasmPackageName,
	})

	if err != nil {
		return nil, err
	}

	// Create the new Compute service stack at Fastly.
	compute, err := fastly.NewServiceCompute(context, "fillip-edge-compute", &fastly.ServiceComputeArgs{
		Activate: pulumi.Bool(true), // We're going to activate the service immediately.
		Backends: &fastly.ServiceComputeBackendArray{
			&fastly.ServiceComputeBackendArgs{
				Address:             pulumi.String("home-fillip-pro.deno.dev"), // Our Deno home web application.
				Port:                pulumi.Int(443),                           // TLS, otherwise we'll get redirects from port 80.
				BetweenBytesTimeout: pulumi.Int(10000),
				ConnectTimeout:      pulumi.Int(1000),
				ErrorThreshold:      pulumi.Int(0),
				FirstByteTimeout:    pulumi.Int(15000),
				Name:                pulumi.String("home_fillip_pro"),          // Name our backend to reference in the WebAssembly code.
				OverrideHost:        pulumi.String("home-fillip-pro.deno.dev"), // Override the Host header to enable Deno to identify where to route traffic.
				SslCertHostname:     pulumi.String("*.deno.dev"),               // Ensure we're routed correctly by referencing the certificate host name.
				UseSsl:              pulumi.Bool(true),
			},
			&fastly.ServiceComputeBackendArgs{
				Address:             pulumi.String("dash-fillip-pro.deno.dev"), // Our Deno dashboard web application.
				Port:                pulumi.Int(443),                           // TLS, otherwise we'll get redirects from port 80.
				BetweenBytesTimeout: pulumi.Int(10000),
				ConnectTimeout:      pulumi.Int(1000),
				ErrorThreshold:      pulumi.Int(0),
				FirstByteTimeout:    pulumi.Int(15000),
				Name:                pulumi.String("dash_fillip_pro"),          // Name our backend to reference in the WebAssembly code.
				OverrideHost:        pulumi.String("dash-fillip-pro.deno.dev"), // Override the Host header to enable Deno to identify where to route traffic.
				SslCertHostname:     pulumi.String("*.deno.dev"),               // Ensure we're routed correctly by referencing the certificate host name.
				UseSsl:              pulumi.Bool(true),
			},
		},
		Domains: &fastly.ServiceComputeDomainArray{
			&fastly.ServiceComputeDomainArgs{
				Name: pulumi.String("fillip.pro"),
			},
		},
		Reuse: pulumi.Bool(true), // Re-use an existing service to prevent pulling a service down.
		Name:  pulumi.String(serviceName),
		/*LoggingS3s: fastly.ServiceComputeLoggingS3Array{
			fastly.ServiceComputeLoggingS3Args{
				BucketName:       pulumi.String("private.eu.fillip.pro"),
				CompressionCodec: pulumi.String("gzip"),
				Domain:           pulumi.String("s3.eu-central-2.wasabisys.com"),
				FileMaxBytes:     pulumi.Int(256),
				MessageType:      pulumi.String("classic"),
				Name:             pulumi.String("fillip-log-store"),
				Path:             pulumi.String(".logs/edge-network/"),
				Period:           pulumi.Int(3600),
			},
		},*/
		Package: fastly.ServiceComputePackageArgs{ // Our WebAssembly package.
			Filename:       pulumi.String(wasmPackageName),
			SourceCodeHash: pulumi.String(hash.Hash),
		},
		ResourceLinks: &fastly.ServiceComputeResourceLinkArray{ // Create a new link between this service and our existing KV store.
			&fastly.ServiceComputeResourceLinkArgs{
				Name:       pulumi.String("kv.eu.fillip.pro-link"),
				ResourceId: kvStoreId.AsStringOutput(),
			},
		},
		Comment: pulumi.String("Automatically provisioned by the Enso Composer"),
	})

	if err != nil {
		return compute, err
	}

	return compute, err
}

// Get the WebAssembly compute package from storage. We're pushing our WebAssembly modules
// to an S3-based store (via Wasabi) as part of our CI process.
//
// This function will retrieve that package.
func getComputePackage() (string, error) {

	// TODO: Move necessary and common credentials to the root of the program
	// to prevent the program getting this far before checking if it can complete.
	var (
		ACCESS_KEY = os.Getenv("ENSO_S3_ACCESS_KEY")
		SECRET_KEY = os.Getenv("ENSO_S3_SECRET_KEY")

		REGION   = "eu-central-2"
		ENDPOINT = "https://s3.eu-central-2.wasabisys.com"
	)

	// TODO: We should have a common context at this point.
	context := context.Background()

	// We're not using AWS as a provider, but we are using its SDK.
	//
	// We need to create a new static credentials provider to utilize the environment variables as
	// credentials.
	awsConfiguration, err := config.LoadDefaultConfig(context,
		config.WithRegion(REGION),
		config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(ACCESS_KEY, SECRET_KEY, "")),
	)

	// Reference the Wasabi endpoint (e.g. `https://s3.eu-central-2.wasabisys.com`).
	*awsConfiguration.BaseEndpoint = ENDPOINT

	// Amend the configuration to disable the path style for Wasabi compatibility, and
	// generate the S3 s3Client.
	s3Client := s3.NewFromConfig(awsConfiguration, func(o *s3.Options) {
		o.UsePathStyle = false
	})

	if err != nil {
		return "", err
	}

	// Create a new home baked S3 runner.
	s3Runner := &assets.BucketBasics{
		S3Client: s3Client,
	}

	// Retrieve the file from storage.
	//
	// TODO: make this configurable externally and set the output as a consistent variable to prevent the stack and the download
	// mismatching later.
	fileName, err := s3Runner.DownloadFile("fillip.pro", "enso/assets/compute/fillip-fastly-router.tar.gz", ".enso-router.tar.gz")

	return fileName, err
}
