package state

import (
	"fmt"
	"os"

	"github.com/pulumi/pulumi/sdk/v3/go/common/tokens"
	"github.com/pulumi/pulumi/sdk/v3/go/common/workspace"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Project structure for Pulumi projects.
type Project struct {
	// Labels to differentiate and easily find a given group of projects.
	Labels *pulumi.StringMap
	// Name of the project.
	Name string
}

// Creates a new `Project` with a given name to be used when managing the Pulumi state.
func CreateProject(name string) *Project {
	return &Project{
		Labels: nil,
		Name:   name,
	}
}

// Get (or create) a Pulumi project based on the configuration
func (project Project) GetPulumiProject() (*workspace.Project, error) {
	wasabiBucket := os.Getenv("ENSO_S3_BUCKET")

	// Set up the Wasabi S3 backend options.
	backendURL := fmt.Sprintf("s3://%s", wasabiBucket)

	workspaceProject := &workspace.Project{
		Name:    tokens.PackageName(project.Name),
		Runtime: workspace.NewProjectRuntimeInfo("go", nil),
		Backend: &workspace.ProjectBackend{URL: backendURL},
	}

	return workspaceProject, nil
}
