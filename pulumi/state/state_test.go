package state

import (
	"testing"
)

// TestGuaranteeLocalState tests the creation of local state.
func TestGuaranteeLocalState(t *testing.T) {
	path := ".tests"
	_, err := GuaranteeLocalState(path)

	if err != nil {
		t.Fail()
	}
}
