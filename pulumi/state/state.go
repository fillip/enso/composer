package state

import (
	"os"
)

// GuaranteeLocalState ensures there's a local state present.
func GuaranteeLocalState(path string) (string, error) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {

		err = os.MkdirAll(path, 0755)
		if err != nil {
			return "", err
		}

		return path, err
	}

	return path, nil
}
