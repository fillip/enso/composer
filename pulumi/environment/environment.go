package environment

import (
	"context"
	"fmt"
	"os"

	"github.com/blang/semver"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
)

// Installs Pulumi onto the base system for invocation on the terminal.
func Install(context context.Context) (*auto.PulumiCommand, error) {
	tempDir, err := os.MkdirTemp("", "pulumi_cli")

	if err != nil {
		fmt.Printf("Failed to create temp dir: %v\n", err)
		os.Exit(1)
	}

	// Install pulumi v3.104.1 into $tmpDir
	pulumiCommand, err := auto.InstallPulumiCommand(context, &auto.PulumiCommandOptions{
		// Version defaults to the version matching the current pulumi/sdk.
		Version: semver.MustParse("3.122.0"),
		// Root defaults to `$HOME/.pulumi/versions/$VERSION`.
		Root: tempDir,
	})

	return &pulumiCommand, err
}

// Gets the Pulumi command for terminal invocation.
func GetCommand(context context.Context) (*auto.PulumiCommand, error) {
	pulumiCommand := context.Value("pulumi_command").(*auto.PulumiCommand)

	return pulumiCommand, nil
}
