package stack

import (
	"os"
	"testing"

	"github.com/pulumi/pulumi/sdk/v3/go/common/resource"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// TestStack tests the setting up and execution of a sample Pulumi stack.
func TestStack(t *testing.T) {
	os.Setenv("PULUMI_CONFIG", "{ \"project\": \"sumi\" }")

	os.Setenv("PULUMI_PROJECT", "sumi")
	os.Setenv("PULUMI_STACK", "sumi-dev")
	os.Setenv("PULUMI_MONITOR", "127.0.0.1")
	os.Setenv("PULUMI_ENGINE", "127.0.0.1")

	config := map[string]string{
		"project:example.foo": "goof",
		"project:example.bar": "gold",
	}

	err := pulumi.RunErr(func(ctx *pulumi.Context) error {
		return nil
	}, WithMocksAndConfig("sumi", "sumi-dev", config, mocks(0)))

	if err != nil {
		t.Fail()
	}
}

// Mocks is a type alias for mocking the Pulumi process.
type mocks int

// NewResource creates a new mock resource in the Pulumi stack.
func (mocks) NewResource(args pulumi.MockResourceArgs) (string, resource.PropertyMap, error) {
	return args.Name + "_id", args.Inputs, nil
}

// Call mocks the `Call` method for running a Pulumi stack.
func (mocks) Call(args pulumi.MockCallArgs) (resource.PropertyMap, error) {
	return args.Args, nil
}

// Duplicate the pulumi.WithMocks() function with the additional configuration
// value of 'config'.
func WithMocksAndConfig(project, stack string, config map[string]string, mocks pulumi.MockResourceMonitor) pulumi.RunOption {
	return func(info *pulumi.RunInfo) {
		info.Project, info.Stack, info.Mocks, info.Config = project, stack, mocks, config
	}
}
