package stack

import (
	"context"
	"os"

	"git.fillip.dev/enso/composer/pulumi/state"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// Stack provides a portable structure for state information when managing the stack.
type Stack struct {
	// Project provides a reference to the project information when managing the stack.
	Project *state.Project

	// Identiy for the organization
	OrganizationIdentity string

	// Project name for the stack.
	ProjectName string

	// Name is semantic tag of the stack to be used when differentiating environments.
	EnvironmentName string

	// DeployFunc is the Pulumi program function which defines the contents of the project
	DeployFunc func(pulumiContext *pulumi.Context) error
}

type StackComposer interface {
	ApplyStack(context context.Context) error
	CreateStack(context context.Context) (*auto.Stack, error)
}

// Sets the common configuration across modules, including setting up the remote storage
// to ensure the state is stored in the same place.
func SetBaseConfig(context context.Context, stack *auto.Stack) error {
	// Fetch Wasabi S3 credentials from environment variables.
	wasabiAccessKey := os.Getenv("ENSO_S3_ACCESS_KEY")
	wasabiSecretKey := os.Getenv("ENSO_S3_SECRET_KEY")
	wasabiRegion := os.Getenv("ENSO_S3_REGION")

	// Set Wasabi S3 configuration for the stack.
	configMap := auto.ConfigMap{
		"aws:accessKey":                 auto.ConfigValue{Value: wasabiAccessKey},
		"aws:secretKey":                 auto.ConfigValue{Value: wasabiSecretKey},
		"aws:region":                    auto.ConfigValue{Value: wasabiRegion},
		"aws:skipCredentialsValidation": auto.ConfigValue{Value: "true"},
		"aws:skipRequestingAccountId":   auto.ConfigValue{Value: "true"},
		"wasabi:accessKey":              auto.ConfigValue{Value: wasabiAccessKey},
		"wasabi:secretKey":              auto.ConfigValue{Value: wasabiSecretKey},
		"wasabi:region":                 auto.ConfigValue{Value: wasabiRegion},
	}

	for key, value := range configMap {
		err := stack.SetConfig(context, key, value)
		if err != nil {
			return err
		}
	}

	return nil
}
