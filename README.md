# Enso Composer

A single binary composer for the [Fillip.pro](https://fillip.pro) web content using [Pulumi](https://pulumi.com) and written in [Go](https://go.dev)

## Deployment

To deploy an environment, the relevant environment variables firstly need to be set. The following is a comprehensive and complete list of the environment variables the composer is expecting.

| Variable                      | Description                                                               | Example |
| :---                          |     :---                                                                  | :--- |
| `AWS_DEFAULT_REGION`          | Default region for the S3 state storage.                                  | `eu-central-2` |
| `AWS_ACCESS_KEY_ID`           | Access key for the S3-compliant state storage for Pulumi.                 | `...` |
| `AWS_SECRET_ACCESS_KEY`       | Secret key for the S3-compliant state storage for Pulumi.                 | `...` |
| `AWS_ENDPOINT_URL`            | AWS S3-compliant endpoint URL for S3 storage of the Pulumi state.         | `"https://s3.<region_name>.wasabisys.com"`      |
| `ENSO_S3_ACCESS_KEY`          | Access key for the S3-compliant state storage for Pulumi.                 | `...`|
| `ENSO_S3_SECRET_KEY`          | Secret key for the S3-compliant state storage for Pulumi.                 | `...` |
| `ENSO_S3_REGION`              | Region for the S3-compliant state storage for Pulumi.                     | `eu-central-2` |
| `ENSO_S3_BUCKET`              | Bucket to store the Pulumi state in.                                      | `<bucket_name>?endpoint=s3.<region_name>.wasabisys.com&disableSSL=true&s3ForcePathStyle=true` |
| `FASTLY_S3_IAM_ROLE`          | Fastly S3 IAM role for connecting an S3 backend to Fastly.                | `arn:aws:iam::100000214856:role/<role_name>`    |
| `FASTLY_API_TOKEN`            | API token from Fastly for deploying the infrastructure. This must be an automation token, not a user token. | `...` |
| `GANDI_PAT`                   | Gandi personal access token (PAT) for accessing the Gandi services.       | `...` |
| `PULUMI_CONFIG_PASSPHRASE`    | Passphrase for securing the Pulumi state.                                 | `🤫` |

